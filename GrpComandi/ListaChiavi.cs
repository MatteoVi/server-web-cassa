﻿using Server.GrpUtility;

namespace Server.GrpComandi
{
    internal class ListaChiavi
    {
        /// <summary>
        ///     Restituisce l'elenco delle chiavi di una sezione fra tag "option"
        /// </summary>
        /// <param name="cookieId">CookieId</param>
        /// <param name="parTag">Parametro tag</param>
        /// <param name="chiave">Chiave da cercare</param>
        /// <returns></returns>
        internal string Lista(long cookieId, string parTag, string chiave)
        {
            // Ricevo la lista delle chiavi dal file INI
            var ini = new IniFile("server.ini");
            var lista = ini.ListaChiavi(chiave);

            // Se devo impostare il valore precedentemente assegnato, cerco nel file ini
            var valPrec = string.Empty;
            if (parTag.UgualeA("true"))
                valPrec = ini.Leggi(cookieId.ToString(), chiave);

            if (valPrec.UgualeA("all"))
                valPrec = string.Empty;

            var sb = new System.Text.StringBuilder();
            // Inserisco per primo il valore "Tutte le categorie" nel caso non ci siano valori predefiniti
            var tagSet = string.IsNullOrEmpty(valPrec);
            sb.AppendFormat("<option {0} value=\"all\">Tutte</option>", tagSet ? "selected=\"selected\"" : string.Empty);

            // Aggiungo le altre chiavi ed imposto il predefinito
            var tagSetB = tagSet;
            var num = lista.Length;
            for (var k = 0; k < num; k++)
            {
                if (string.IsNullOrEmpty(lista[k]))
                    continue;

                if (!tagSetB)
                    tagSet = lista[k].UgualeA(valPrec);

                sb.AppendFormat("<option {1} value=\"{0}\">{0}</option>", lista[k], tagSet && !tagSetB ? "selected=\"selected\"" : string.Empty);

                if (tagSet)
                    tagSetB = true;
            }

            // Se non ho selezionato nulla di predefinito aggiungo una riga con "personalizzato" selezionato
            if (!tagSet)
                sb.Insert(0, "<option selected=\"selected\" value=\"\">Personale</option>");

            return sb.ToString();
        }
    }
}
