﻿using System.Text;
using FirebirdSql.Data.FirebirdClient;

namespace Server.GrpComandi
{
    internal class Setupk
    {
        private int _comNr;

        /// <summary>
        ///     Crea una pagina di setup postazione
        /// </summary>
        /// <param name="cookieId">Cookie di sessione</param>
        /// <returns></returns>
        internal string CreaPagina(ref long cookieId)
        {
            return TrasformaInHtml(ref cookieId);
        }

        private string TrasformaInHtml(ref long cookieId)
        {
            // Ricavo il numero delle comande
            _comNr = NumeroComande();
            if (_comNr == 0)
                return string.Empty;

            // Ricavo i nomi delle comande
            var comNome = new string[_comNr];
            NomiComande(ref comNome);

            // Creo un array per segnare lo stato delle comande
            var comSts = new bool[_comNr];

            // Leggo dalla cache se ci sono già delle comande impostate e normalizzo il valore
            var par = GrpCache.CacheParametri.Get(cookieId, "orderknr");
            GrpCache.Utility.NormalizzaParametri(ref par, "orderknr");

            // Nel caso ci siano parametri salvati
            if (!string.IsNullOrEmpty(par))
            {
                // Suddivido i singoli valori ponendoli in un array temporaneo
                var tmp = par.Split('-');

                // Ciclo su tutti i valori
                var tmpNr = tmp.Length;
                for (var t = 0; t < tmpNr; t++)
                {
                    // Converto il valore stringa in integer e riconduco il valore a base zero
                    var val = int.Parse(tmp[t]) - 1;
                    // se è un valore compreso nell'intervallo delle comande disponibili
                    if (_comNr > val)
                        // dico che è selezionato
                        comSts[val] = true;
                }
            }

            // Costruisco la pagina del setup
            var html = new StringBuilder();
            html.Append("<div class=\"cassa-bi\"><form class=\"pure-form\">");
            html.Append("<table class=\"pure-table\"><thead><tr><th class=\"column1\">Comanda</th><th class=\"column2\">Visibile</th></tr></thead><tbody>");

            for (var c = 0; c < _comNr; c++)
            {
                html.AppendFormat("\n<tr{0}>", (c + 1) % 2 == 0 ? " class=\"pure-table-odd\"" : string.Empty);
                html.AppendFormat("<td>{0}</td><td><input autocomplete=\"off\" type=\"checkbox\" name=\"{1}\" value=\"{1}\"{2}></td></tr>", comNome[c], c + 1, comSts[c] ? " checked" : string.Empty);
            }

            html.Append("\n</tbody></table></form>\n<button id=\"save\" class=\"button-xlarge pure-button button-green pure-button-fa\">Salva dati comande</button></div>");
            return html.ToString();
        }

        /// <summary>
        ///     Restituisce il numero di comande
        /// </summary>
        /// <returns></returns>
        private static int NumeroComande()
        {
            const string sql = "select DATA1 from CONFIG where GRP=7 and SEQ=0;";

            var myConnection = new FbConnection(GrpDatabase.GestioneDb.StringaConnessione);
            myConnection.Open();
            var myCommand = new FbCommand(sql, myConnection);
            var myDataReader = myCommand.ExecuteReader();
            myDataReader.Read();

            int nrComande;
            try
            {
                int.TryParse(myDataReader.GetString(0), out nrComande);
            }
            catch
            {
                nrComande = 1;
            }

            myDataReader.Close();
            myDataReader.Dispose();
            myCommand.Dispose();
            myConnection.Close();
            myConnection.Dispose();

            return nrComande;
        }

        /// <summary>
        ///     Restituisce un array con il nome delle comande
        /// </summary>
        /// <returns></returns>
        private void NomiComande(ref string[] nomi)
        {
            var myConnection = new FbConnection(GrpDatabase.GestioneDb.StringaConnessione);
            myConnection.Open();

            for (var c = 0; c < _comNr; c++)
            {
                var sql = "select DATA1 from CONFIG where GRP=7 and SEQ=" + (c + 10) + 3 + ";";
                var myCommand = new FbCommand(sql, myConnection);
                var myDataReader = myCommand.ExecuteReader();
                myDataReader.Read();

                try
                {
                    nomi[c] = myDataReader.GetString(0);
                }
                catch
                {
                    nomi[c] = "COMANDA " + (c + 1);
                }

                myDataReader.Close();
                myDataReader.Dispose();
                myCommand.Dispose();
            }

            myConnection.Close();
            myConnection.Dispose();
        }
    }
}
