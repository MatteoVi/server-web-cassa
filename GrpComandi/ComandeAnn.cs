﻿using System;
using Server.GrpUtility;
using FirebirdSql.Data.FirebirdClient;

namespace Server.GrpComandi
{
    internal class ComandeAnn
    {
        /// <summary>
        ///     Elabora le comande
        /// </summary>
        /// <param name="cookieId">Cookie di sessione</param>
        /// <param name="parUrl">Parametro passato nell'url</param>
        /// <returns></returns>
        internal string ElaboraComande(ref long cookieId, ref string parUrl)
        {
            string tmp;
            if (!parUrl.TrovaValore("timestamp", out tmp))
                return string.Empty;
            var tsP = Convert.ToInt64(tmp);

            if (!parUrl.TrovaValore("orderknr", out tmp))
                return string.Empty;

            var nrComande = string.Empty;
            if (!string.IsNullOrEmpty(tmp))
                nrComande = " IDKORDER in (" + tmp.Replace('-', ',') + ") and";

            var sql = "select IDTICKETL, DATETIME from TICKETK where" + nrComande + " HIDE='F' and STATUS='A'";

            var myConnection = new FbConnection(GrpDatabase.GestioneDb.StringaConnessione);
            myConnection.Open();
            var myCommand = new FbCommand(sql, myConnection);
            var myDataReader = myCommand.ExecuteReader();

            var refresh = false;
            while (myDataReader.Read())
            {
                var tsK = Varie.TimeStamp(myDataReader.GetDateTime(1));
                if (tsK <= tsP)
                    continue;
                refresh = true;
                break;
            }

            myDataReader.Close();
            myDataReader.Dispose();
            myCommand.Dispose();
            myConnection.Close();
            myConnection.Dispose();

            Console.WriteLine("Need refresh? " + refresh.ToString());
            return refresh ? "true" : "false";
        }
    }
}