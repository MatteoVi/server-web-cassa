﻿using Server.GrpDatabase;
using Server.GrpUtility;

namespace Server.GrpComandi
{
    internal class Azioni
    {
        private readonly string _par;
        private readonly long _cookieId;

        /// <summary>
        ///     Pone tutti i prodotti in stato evaso ad eccezione di quelli annullati e li nasconde alla vista
        /// </summary>
        internal void PrdUpdStatoChiudi()
        {
            string numC;
            if (!_par.TrovaValore("orderk", out numC))
                return;

            if (!string.IsNullOrEmpty(numC))
                numC = "IDKORDER in (" + numC.Replace('-', ',') + ") and";

            string idSc;
            if (!_par.TrovaValore("idticketh", out idSc))
                return;

            var sql = "update TICKETK set STATUS='E' where " + numC + " IDTICKETH=" + idSc + " and (STATUS='P' or STATUS='S')";
            GestioneDb.ExecSql(sql);

            sql = "update TICKETK set DATETIME=CURRENT_TIMESTAMP, HIDE='T' where " + numC + " IDTICKETH=" + idSc + ";";
            GestioneDb.ExecSql(sql);
        }

        /// <summary>
        ///     Pone i prodotti sospesi (S) in stato pendente (P)
        /// </summary>
        internal void PrdUpdStatoP()
        {
            string numC;
            if (!_par.TrovaValore("orderk", out numC))
                return;

            if (!string.IsNullOrEmpty(numC))
                numC = "IDKORDER in (" + numC.Replace('-', ',') + ") and";

            string idSc;
            if (!_par.TrovaValore("idticketh", out idSc))
                return;

            var sql = "update TICKETK set STATUS='P', DATETIME=CURRENT_TIMESTAMP where " + numC + " IDTICKETH=" + idSc + " and STATUS='S';";
            GestioneDb.ExecSql(sql);
        }

        /// <summary>
        ///     Aggiorna i dati del tavolo
        /// </summary>
        internal void UpdTavolo()
        {
            string idSc;
            if (!_par.TrovaValore("id", out idSc))
                return;

            string table;
            if (!_par.TrovaValore("table", out table))
                return;

            var sql = "update TICKETH set DATA3='" + table + "' where IDTICKETH=" + idSc + ";";
            GestioneDb.ExecSql(sql);
        }

        /// <summary>
        ///     Aggiorna lo stato del prodotto quando viene premuto il check
        /// </summary>
        /// <param name="stato"></param>
        internal void PrdUpdStato(char stato)
        {
            var statoInv = 'P';
            if (stato == 'P') statoInv = 'E';
            string idR;
            if (!_par.TrovaValore("idticketl", out idR))
                return;

            var sql = "update TICKETK set STATUS='" + stato + "', DATETIME=CURRENT_TIMESTAMP where IDTICKETL=" + idR + " and STATUS='" + statoInv + "';";
            GestioneDb.ExecSql(sql);
        }

        /// <summary>
        ///     Salvo i dati nella cache
        /// </summary>
        /// <param name="salva">Se true, salva su file</param>
        internal void ScriviParametri(bool salva)
        {
            // Rimuovo la stringa dell'azione dal dato vero e proprio
            var rem = salva ? "act=a_setparf" : "act=a_setpar";
            var i = _par.IndexOf(rem);
            var f = i + rem.Length - 1;
            if (_par.Length > f && _par[f + 1] == '&')
                rem += '&';
            var dati = _par.Replace(rem, string.Empty).Trim();

            // Scrivo i dati nella cache
            if (!string.IsNullOrEmpty(dati))
                GrpCache.CacheParametri.Set(_cookieId, dati, salva);
        }

        internal Azioni(ref string par, ref long cookieId)
        {
            _par = par;
            _cookieId = cookieId;
        }
    }
}
