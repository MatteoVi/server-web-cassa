﻿namespace Server.GrpComandi
{
    internal class Comanda
    {
        /// <summary>
        ///     Codice univoco scontrino
        /// </summary>
        internal long ScontrinoId;

        /// <summary>
        ///     Numero scontrino
        /// </summary>
        internal int ScontrinoNum;

        /// <summary>
        ///     Tavolo
        /// </summary>
        internal string ScontrinoTavolo;

        /// <summary>
        ///     Note scontrino
        /// </summary>
        internal string ScontrinoNote;

        /// <summary>
        ///     Codice univoco riga scontrino
        /// </summary>
        internal long ScontrinoIdRiga;

        /// <summary>
        ///     Codice univoco prodotto
        /// </summary>
        internal long ProdId;

        /// <summary>
        ///     Quantità prodotto
        /// </summary>
        internal decimal ProdQta;

        /// <summary>
        ///     Note prodotto
        /// </summary>
        internal string ProdNote;

        /// <summary>
        ///     Stato riga: P = Pendente | S = Sospesa | A = Annullata | E = Evasa
        /// </summary>
        internal char ProdStato;

        /// <summary>
        ///     Nome prodotto
        /// </summary>
        internal string ProdNome;

        /// <summary>
        ///     Nome famiglia
        /// </summary>
        internal string FamNome;

        /// <summary>
        ///     Unità di misura
        /// </summary>
        internal string ProdUm;

        /// <summary>
        ///     Codice raggruppamento menù
        /// </summary>
        internal long IdMenuGrp;

        /// <summary>
        ///     Nome del menù
        /// </summary>
        internal string MenuNome;

        /// <summary>
        ///     Inizializza comanda
        /// </summary>
        /// <param name="scontrinoId">Codice univoco scontrino</param>
        /// <param name="scontrinoNum">Numero scontrino</param>
        /// <param name="tavolo">Tavolo</param>
        /// <param name="scontrinoNote">Note scontrino</param>
        /// <param name="scontrinoIdRiga">Codice univoco riga scontrino</param>
        /// <param name="idProduct">Codice univoco prodotto</param>
        /// <param name="quantita">Quantità totale</param>
        /// <param name="noteRiga">Note riga</param>
        /// <param name="prodStato">Riga evasa</param>
        internal Comanda(long scontrinoId, int scontrinoNum, string tavolo, string scontrinoNote, long scontrinoIdRiga, long idProduct, decimal quantita, string noteRiga, char prodStato, long idMenuList, long idMenuGrp)
        {
            ScontrinoId = scontrinoId;
            ScontrinoNum = scontrinoNum;
            ScontrinoTavolo = tavolo;
            ScontrinoNote = scontrinoNote;
            ScontrinoIdRiga = scontrinoIdRiga;
            ProdId = idProduct;
            ProdQta = quantita;
            ProdNote = noteRiga;
            ProdStato = prodStato;
            IdMenuGrp = idMenuGrp;

            // Completo i dati accedendo alla cache
            GrpCache.CacheProdotti.Get(idProduct, out ProdNome, out FamNome, out ProdUm);

            if (idMenuList > 0)
                GrpCache.CacheMenu.Get(idMenuList, out MenuNome);
        }
    }
}
