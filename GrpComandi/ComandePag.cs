﻿using System.Text;
using FirebirdSql.Data.FirebirdClient;
using System.Collections.Generic;

namespace Server.GrpComandi
{
    internal class ComandePag
    {
        private readonly List<Comanda> _item = new List<Comanda>();

        /// <summary>
        ///     Elabora le comande
        /// </summary>
        /// <param name="cookieId">Cookie di sessione</param>
        /// <param name="parTag">Override comande</param>
        /// <param name="parUrl">Override comande da riga url</param>
        /// <returns></returns>
        internal string ElaboraComande(ref long cookieId, ref string parTag, ref string parUrl)
        {
            string par;

            // Prima considero l'URL, poi ciò che è presente nel tag cassa, poi prendo da cache
            if (!string.IsNullOrEmpty(parUrl))
                par = parUrl;
            else if (!string.IsNullOrEmpty(parTag))
                par = parTag;
            else
                par = GrpCache.CacheParametri.Get(cookieId, "orderknr");

            GrpCache.Utility.NormalizzaParametri(ref par, "orderknr");
            GrpCache.CacheParametri.Set(cookieId, "orderknr=" + par);

            // Interroga il database
            return LeggiDb(par) ? TrasformaInHtml(ref par) : string.Empty;
        }

        private const string ChiudiTabella = "</tbody></table></div></div></div>\n";

        private string TrasformaInHtml(ref string nrComande)
        {
            var html = new StringBuilder();
            html.AppendFormat("<div id=\"orderk\" value=\"{0}\"></div>\n", nrComande);

            long scontrinoId = -1;
            var riga = 0;
            var usoBtnGiallo = false;
            var btnGiallo = string.Empty;

            var num = _item.Count;
            for (var c = 0; c < num; c++)
            {
                var item = _item[c];

                // Al cambio scontrino, metto i pulsanti laterali
                if (scontrinoId != item.ScontrinoId)
                {
                    if (c != 0)
                    {
                        html.Replace("*BG*", usoBtnGiallo ? btnGiallo : string.Empty);
                        html.Append(ChiudiTabella);
                        usoBtnGiallo = false;
                    }

                    scontrinoId = item.ScontrinoId;
                    riga = 0;

                    html.AppendFormat("<div id=\"{0}\" class=\"cassa-bi\"><div class=\"cassa-table-title\">", scontrinoId);
                    html.AppendFormat("<div class=\"cassa-sc-num\">Scontrino nr. <span>{0}</span></div>", item.ScontrinoNum);
                    html.AppendFormat("<div class=\"cassa-sc-table\"><div class=\"pure-form\"> Tavolo: <input autocomplete=\"off\" type=\"text\" maxlength=\"20\" name=table value=\"{0}\"><button type=\"submit\" class=\"pure-button button-orange pure-button-fa\" onClick=\"getInput('a_sctabl', '{1}', 'null')\"><i class=\"fa fa-floppy-o\"></i></button></div></div>", item.ScontrinoTavolo, scontrinoId);

                    if (!string.IsNullOrEmpty(item.ScontrinoNote))
                        html.AppendFormat("<div class=\"cassa-sc-note\">{0}</div>", item.ScontrinoNote);

                    html.Append("</div><div class=\"cassa-table-body\"><div class=\"cassa-table-body-left\">");
                    html.AppendFormat("<form class=\"pure-form\" method=get action=\"orderk.html#{0}\"><button type=\"submit\" name=act value=a_screm class=\"button-xlarge pure-button button-green pure-button-fa\"><span class=\"fa-stack fa-lg\"><i class=\"fa fa-eye fa-stack-1x\"></i><i class=\"fa fa-ban fa-stack-2x text-red-ban\"></i></span></button>*BG* <input type=\"hidden\" name=idticketh value={0}><input type=\"hidden\" name=orderk value={1}>", scontrinoId, nrComande);
                    html.Append("</form></div><div class=\"cassa-table-body-right\"><table class=\"pure-table\"><thead><tr><th class=\"column1\">#</th><th class=\"column2\">Stato</th><th class=\"column3\">Prodotto</th><th class=\"column4\">Note</th><th class=\"column5\">U.M.</th><th class=\"column6\">Qt&agrave;</th><th class=\"column7\">Evaso</th></tr></thead><tbody>");

                    btnGiallo = "<br /><button type=\"submit\" name=act value=a_scena class=\"button-xlarge pure-button button-yellow pure-button-fa cassa-sec-button\"><i class=\"fa fa-play\"></i></button>";
                }

                riga++;

                // Stabilisco se devo mettere la riga per suddividere i menù
                var rigaMenuUp = false;
                var rigaMenuDown = false;
                if (item.IdMenuGrp != 0)
                {
                    rigaMenuUp = c == 0 ? true : _item[c - 1].IdMenuGrp != item.IdMenuGrp;
                    rigaMenuDown = c == num - 1 ? true : _item[c + 1].ScontrinoId != item.ScontrinoId;
                }

                if (rigaMenuUp)
                    html.AppendFormat("\n<tr class=\"cassa-riga-menu-up\"><td colspan=\"7\">{0}</td></tr>", item.MenuNome);

                html.AppendFormat("\n<tr id=\"{0}\" class=\"{1} {2}\">", item.ScontrinoIdRiga, riga%2 == 0 ? "pure-table-odd" : string.Empty, rigaMenuDown ? "cassa-riga-menu-down" : string.Empty);

                html.AppendFormat("<td>{0}</td>", riga);
                if (item.ProdStato == 'P')
                    html.AppendFormat("<td class=\"cassa-icon cassa-sts-blue\"><i class=\"fa fa-clock-o\"></i></td>");
                else if (item.ProdStato == 'S')
                {
                    usoBtnGiallo = true;
                    html.AppendFormat("<td class=\"cassa-icon cassa-sts-yellow\"><i class=\"fa fa-pause\"></i></td>");
                }
                else if (item.ProdStato == 'A')
                    html.AppendFormat("<td class=\"cassa-icon cassa-sts-red\"><span class=\"blink_me\"><i class=\"fa fa-exclamation-triangle\"></i></span></td>");
                else if (item.ProdStato == 'E')
                    html.AppendFormat("<td class=\"cassa-icon cassa-sts-green\"><i class=\"fa fa-check\"></i></td>");
                else
                    html.AppendFormat("<td><i class=\"fa fa-minus\"></i></td>");

                html.AppendFormat("<td>{0}</td><td>{1}</td><td class=\"align-center\">{2}</td><td class=\"align-right\">{3}</td><td class=\"cassa-icon cassa-icon-align\">", item.ProdNome, item.ProdNote, item.ProdUm, item.ProdQta);

                if (item.ProdStato == 'P' || item.ProdStato == 'E')
                    html.AppendFormat("<input autocomplete=\"off\" type=\"checkbox\" value=\"{0}\" {1}>", item.ScontrinoIdRiga, item.ProdStato == 'E' ? "checked" : string.Empty);
                //html.AppendFormat("<input autocomplete=\"off\" name=\"checkboxes[]\" type=\"checkbox\" class=\"toggle-checkbox\" value=\"{0}\" {1}>", item.ScontrinoIdRiga, item.ProdStato == 'E' ? "checked" : string.Empty);
                else if (item.ProdStato == 'A')
                    html.Append("<span class=\"cassa-sts-red\">ANNULLATO</span>");

                html.Append("</td></tr>");
            }

            html.Replace("*BG*", usoBtnGiallo ? btnGiallo : string.Empty);
            html.Append(ChiudiTabella);
            //html.Append("<script>$('.toggle-checkbox').toggleCheckbox(['<i class=\"fa fa-circle-o\"></i>','<i class=\"fa fa-check-circle-o\"></i>'], function(e, targetCheckbox){checkProd(targetCheckbox);});</script>");

            return html.ToString();
        }

        /// <summary>
        ///     Legge il db e popola l'array delle comande
        /// </summary>
        /// <returns></returns>
        private bool LeggiDb(string nrComande)
        {
            var sql = @"select d.IDTICKETH, d.DATA1 NRSC, d.DATA3 TAV, d.DATA4 NOTESC, c.IDTICKETL, c.IDPRODUCT, c.QTA, c.NOTER, c.STATUS, c.IDMENULIST, c.IDMENUGRP from 
                       (
                        SELECT b.IDTICKETL, b.IDUSER, b.STATUS, b.HIDE, b.IDTICKETH, a.IDPRODUCT, a.DATA2 QTA, a.DATA4 NOTER, 0 IDMENULIST, 0 IDMENUGRP from 
                        TICKETK b left join TICKETL a on (a.IDTICKETL = b.IDTICKETL) 
                        where b.HIDE = 'F' and b.IDMENUGRP=0 {0}
                        union
                        select b.IDTICKETL, b.IDUSER, b.STATUS, b.HIDE, b.IDTICKETH, e.IDPRODUCT, a.DATA2 QTA, a.DATA4 NOTER, a.IDPRODUCT IDMENULIST, b.IDMENUGRP from 
                        TICKETK b left join TICKETL a on (a.IDTICKETL = b.IDMENUGRP) join TICKETLM e on (b.IDTICKETL = e.IDTICKETLM)
                        where b.HIDE = 'F' and b.IDMENUGRP <> 0 {0}
                       ) c left join TICKETH d on (c.IDTICKETH = d.IDTICKETH);";

            if (string.IsNullOrEmpty(nrComande))
                sql = sql.Replace("{0}", string.Empty);
            else
            {
                nrComande = nrComande.Replace('-', ',');
                var sqlCmd = "and b.IDKORDER in (" + nrComande + ")";
                sql = sql.Replace("{0}", sqlCmd);
            }

            var myConnection = new FbConnection(GrpDatabase.GestioneDb.StringaConnessione);
            myConnection.Open();
            var myCommand = new FbCommand(sql, myConnection);
            var myDataReader = myCommand.ExecuteReader();

            try
            {
                while (myDataReader.Read())
                    _item.Add(new Comanda(myDataReader.GetInt64(0),
                                           myDataReader.GetInt32(1),
                                           myDataReader.GetString(2),
                                           myDataReader.GetString(3),
                                           myDataReader.GetInt64(4),
                                           myDataReader.GetInt64(5),
                                           myDataReader.GetDecimal(6),
                                           myDataReader.GetString(7),
                                           myDataReader.GetString(8)[0],
                                           myDataReader.GetInt64(9),
                                           myDataReader.GetInt64(10)
                                           ));
            }
            catch
            {
                return false;
            }

            myDataReader.Close();
            myDataReader.Dispose();
            myCommand.Dispose();
            myConnection.Close();
            myConnection.Dispose();

            return _item.Count > 0;
        }
    }
}
