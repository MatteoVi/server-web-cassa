﻿using System;
using Server.GrpUtility;

namespace Server.GrpComandi
{
    internal class Generici
    {
        /// <summary>
        ///     Restituisce la versione del programma
        /// </summary>
        /// <returns></returns>
        internal static string Versione()
        {
            return "<div class=\"cassa-version\">" + Varie.LeggeVersioneAttuale() + "</div>";
        }

        /// <summary>
        ///     Restituisce la data e l'ora sia in formato timestamp sia formattata
        /// </summary>
        /// <returns></returns>
        internal static string ServerTimeStamp()
        {
            DateTime tsDateTime;
            if (GrpDatabase.GestioneDb.DataOra(out tsDateTime))
                return $"<div id=\"timestamp\" value=\"{Varie.TimeStamp(tsDateTime)}\">{tsDateTime.ToLongTimeString()}</div>";

            return string.Empty;
        }
    }
}
