﻿namespace Server.GrpCache
{
    internal static class CacheCss
    {
        /// <summary>
        ///     Cache CSS
        /// </summary>
        internal static Items Css;

        static CacheCss()
        {
            Css = new Items();
        }
    }
}
