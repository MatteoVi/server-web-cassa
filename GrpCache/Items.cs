﻿using System;

namespace Server.GrpCache
{
    /// <summary>
    ///     Cache generica
    /// </summary>
    internal class Items
    {
        private Item[] _item;

        /// <summary>
        ///     Numero parametri effettivamente presenti
        /// </summary>
        private int _dim;

        /// <summary>
        ///     Aggiunge / Modifica un parametro
        /// </summary>
        /// <param name="chiave">Chiave</param>
        /// <param name="valore">Valore</param>
        internal void AddMod(string chiave, string valore)
        {
            // Se il parametro c'è già, lo aggiorno
            for (var k = 0; k <= _dim; k++)
                if (string.Compare(chiave, _item[k].ChiaveL, StringComparison.OrdinalIgnoreCase) == 0)
                {
                    _item[k].Valore = valore;
                    return;
                }

            // Se il parametro manca, lo aggiungo
            _dim++;
            if (_dim == _item.Length)
                Array.Resize(ref _item, _dim + 5);

            _item[_dim] = new Item(chiave, valore);
        }

        /// <summary>
        ///     Aggiunge / Modifica un parametro
        /// </summary>
        /// <param name="chiave">Chiave</param>
        /// <param name="valore">Valore</param>
        internal void AddMod(long chiave, string valore)
        {
            // Se il parametro c'è già, lo aggiorno
            for (var k = 0; k <= _dim; k++)
                if (chiave == _item[k].ChiaveNum)
                {
                    _item[k].Valore = valore;
                    return;
                }

            // Se il parametro manca, lo aggiungo
            _dim++;
            if (_dim == _item.Length)
                Array.Resize(ref _item, _dim + 5);

            _item[_dim] = new Item(chiave, valore);
        }

        /// <summary>
        ///     Restituisce il valore partendo dalla chiave
        /// </summary>
        /// <param name="chiave">Chiave da cercare in MINUSCOLO</param>
        /// <returns></returns>
        internal string Get(string chiave)
        {
            for (var k = 0; k <= _dim; k++)
                if (string.Compare(chiave, _item[k].ChiaveL, StringComparison.OrdinalIgnoreCase) == 0)
                    return _item[k].Valore;

            return string.Empty;
        }

        /// <summary>
        ///     Restituisce il valore partendo dalla chiave
        /// </summary>
        /// <param name="chiave">Chiave da cercare</param>
        /// <returns></returns>
        internal string Get(long chiave)
        {
            for (var k = 0; k <= _dim; k++)
                if (chiave == _item[k].ChiaveNum)
                    return _item[k].Valore;

            return string.Empty;
        }

        /// <summary>
        ///     Inizializzazione classe
        /// </summary>
        internal Items()
        {
            _dim = -1;
            _item = new Item[3];
        }
    }

}

