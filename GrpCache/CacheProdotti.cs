﻿using FirebirdSql.Data.FirebirdClient;
using Server.GrpDatabase;

namespace Server.GrpCache
{
    static class CacheProdotti
    {
        /// <summary>
        ///     Cache prodotti
        /// </summary>
        private static readonly Items Prodotti;

        /// <summary>
        ///     Recupera i dati del prodotto
        /// </summary>
        /// <param name="chiave">Codice prodotto</param>
        /// <param name="nomeProd">Nome prodotto</param>
        /// <param name="nomeFam">Nome famiglia</param>
        /// <param name="umProd">Unità di misura prodotto</param>
        internal static void Get(long chiave, out string nomeProd, out string nomeFam, out string umProd)
        {
            var res = Prodotti.Get(chiave);
            if (string.IsNullOrEmpty(res))
            {
                CaricaDati(chiave);
                res = Prodotti.Get(chiave);

                if (string.IsNullOrEmpty(res))
                {
                    umProd = "*";
                    nomeProd = "** missing **";
                    nomeFam = "** missing **";
                    return;
                }
            }

            var dati = res.Split('|');
            umProd = dati[0];
            nomeProd = dati[1];
            nomeFam = dati[2];
        }


        private static void CaricaDati(long chiave = -1)
        {
            var sql = "select PRODUCT.IDPRODUCT, PRODUCT.DATA1, PRODUCT.DATA2, FAMILY.DATA1 from PRODUCT left join FAMILY on (PRODUCT.IDFAMILY=FAMILY.IDFAMILY)";

            if (chiave == -1)
                sql += ";";
            else
                sql += " where PRODUCT.IDPRODUCT=" + chiave + ";";

            var myConnection = new FbConnection(GestioneDb.StringaConnessione);
            myConnection.Open();
            var myCommand = new FbCommand(sql, myConnection);
            var myDataReader = myCommand.ExecuteReader();

            while (myDataReader.Read())
            {
                var um = myDataReader.GetByte(1);
                string ums;

                if (um == 0)
                    ums = "Fisso";
                else 
                    ums = "Variab.";

                Prodotti.AddMod(myDataReader.GetInt64(0), ums + '|' + myDataReader.GetString(2) + '|' + myDataReader.GetString(3));
            }

            myDataReader.Close();
            myDataReader.Dispose();
            myCommand.Dispose();
            myConnection.Close();
            myConnection.Dispose();
        }

        static CacheProdotti()
        {
            Prodotti = new Items();
            CaricaDati();
        }
    }
}
