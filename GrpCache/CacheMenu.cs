﻿using FirebirdSql.Data.FirebirdClient;
using Server.GrpDatabase;

namespace Server.GrpCache
{
    class CacheMenu
    {
        /// <summary>
        ///     Cache menù
        /// </summary>
        private static readonly Items Menu;

        /// <summary>
        ///     Recupera i nomi dei menu
        /// </summary>
        /// <param name="chiave">Codice menù</param>
        /// <param name="nomeMenu">Nome menù</param>
        internal static void Get(long chiave, out string nomeMenu)
        {
            var res = Menu.Get(chiave);
            if (string.IsNullOrEmpty(res))
            {
                CaricaDati();
                res = Menu.Get(chiave);

                if (string.IsNullOrEmpty(res))
                {
                    nomeMenu = "** missing **";
                    return;
                }
            }

            nomeMenu = res;
        }

        private static void CaricaDati()
        {
            var sql = "select IDMENULIST, DATA1 from MENULIST";

            var myConnection = new FbConnection(GestioneDb.StringaConnessione);
            myConnection.Open();
            var myCommand = new FbCommand(sql, myConnection);
            var myDataReader = myCommand.ExecuteReader();

            while (myDataReader.Read())
                Menu.AddMod(myDataReader.GetInt64(0), myDataReader.GetString(1));

            myDataReader.Close();
            myDataReader.Dispose();
            myCommand.Dispose();
            myConnection.Close();
            myConnection.Dispose();
        }

        static CacheMenu()
        {
            Menu = new Items();
            CaricaDati();
        }
    }
}