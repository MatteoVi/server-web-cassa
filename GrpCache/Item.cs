﻿namespace Server.GrpCache
{
    /// <summary>
    ///     Item base cache
    /// </summary>
    internal class Item
    {
        internal long ChiaveNum;
        internal string Chiave;
        internal string ChiaveL;
        internal string Valore;

        internal Item(string chiave, string valore)
        {
            Chiave = chiave;
            ChiaveL = chiave.ToLower();
            Valore = valore;
            ChiaveNum = -1;
        }

        internal Item(long chiaveNum, string valore)
        {
            ChiaveNum = chiaveNum;
            Chiave = string.Empty;
            ChiaveL = string.Empty;
            Valore = valore;
        }
    }
}
