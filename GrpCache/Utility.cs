﻿using Server.GrpUtility;

namespace Server.GrpCache
{
    static class Utility
    {
        /// <summary>
        ///     Normalizza i parametri
        /// </summary>
        /// <param name="par">Stringa dei parametri</param>
        /// <returns></returns>
        internal static void NormalizzaParametri(ref string par, string nomePar)
        {
            if (string.IsNullOrEmpty(par))
                return;

            if (par.UgualeA("All"))
            {
                par = string.Empty;
                return;
            }
                
            const char simbolo = '-';
            var nrComande = string.Empty;

            // Creo un array temporaneo dei valori
            var tmpA = par.Split(simbolo);

            // Per ogni valore, verifico se è un numero
            // Se è un numero, accodo il valore a nrComande
            // Se è una stringa, verifico se esiste un controvalore nel file server.ini e accodo a nrComande
            var ini = new IniFile("server.ini");
            var num = tmpA.Length;
            for (var c = 0; c < num; c++)
            {
                int tmpV;
                if (int.TryParse(tmpA[c], out tmpV))
                    nrComande += tmpV.ToString() + simbolo;
                else
                {
                    var str = ini.Leggi(nomePar, tmpA[c]);
                    if (!string.IsNullOrEmpty(str))
                        nrComande += str + simbolo;
                }
            }

            // Dopo aver composto la stringa con i numeri delle comande, devo togliere eventuali doppioni o suddivisioni errate
            num = nrComande.Length;
            var buffer = new char[num];
            var x = 0;
            for (var k = 0; k < num; k++)
                // Se incontro il simbolo di divisione
                if (nrComande[k] == simbolo)
                {
                    // Il divisore non può essere in prima posizione
                    if (x == 0)
                        continue;

                    // Non possono essere presenti due divisori consecutivi
                    if (buffer[x - 1] == simbolo)
                        continue;

                    buffer[x] = simbolo;
                    x++;
                }
                // I numeri li scrivo direttamente
                else
                {
                    buffer[x] = nrComande[k];
                    x++;
                }

            // Elimino l'ultimo divisore se è presente
            if (buffer[x - 1] == simbolo)
                x--;

            // Restituisco solo la parte di buffer di interesse
            par = new string(buffer, 0, x);
        }
    }
}
