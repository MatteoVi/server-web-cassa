﻿namespace Server.GrpCache
{
    internal static class CacheHtml
    {
        /// <summary>
        ///     Cache fogli HTML
        /// </summary>
        internal static Items Html;

        static CacheHtml()
        {
            Html = new Items();
        }
    }
}
