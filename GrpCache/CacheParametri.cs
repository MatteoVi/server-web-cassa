﻿using System;

namespace Server.GrpCache
{
    internal static class CacheParametri
    {
        /// <summary>
        ///     Parametri utente
        /// </summary>
        private static Parametro[] _parClient;

        /// <summary>
        ///     Verifica se esistono parametri per il client e e ne restituisce la posizione nell'array
        /// </summary>
        /// <param name="cookieId">Codice del cookie</param>
        /// <param name="cookieIdx">Indice array dei parametri</param>
        /// <returns>Indice array posizione dato</returns>
        private static bool ParClientEsiste(long cookieId, out int cookieIdx)
        {
            int k;
            var num = _parClient.Length;

            for (k = 0; k < num; k++)
                if (cookieId == _parClient[k].CookieId)
                {
                    cookieIdx = k;
                    return true;
                }

            cookieIdx = -1;
            return false;
        }

        /// <summary>
        ///     Salva i valori dei parametri
        /// </summary>
        /// <param name="cookieId">Codice del cookie</param>
        /// <param name="dati">Stringa dei parametri</param>
        /// <param name="salva">Salva i parametri sul server</param>
        /// <returns>Ritorna l'indice dell'array della cache</returns>
        internal static int Set(long cookieId, string dati, bool salva = false)
        {
            // Stabilisco se esistono già dati per il mio client
            // Se no, creo uno spazio
            // In ogni caso recupero l'indice dell'array
            int idx;
            if (!ParClientEsiste(cookieId, out idx))
            {
                idx = _parClient.Length;
                Array.Resize(ref _parClient, idx + 1);
                _parClient[idx] = new Parametro(cookieId);
            }

            // Suddivido la riga dei parametri e creo un array
            var tmp = dati.Split('&');

            // Per ogni parametro
            var num = tmp.Length;
            for (var k = 0; k < num; k++)
            {
                // Verifico che ci sia l'uguale, se no, ignoro il parametro
                var pos = tmp[k].IndexOf('=');
                if (pos < 1)
                {
                    Console.WriteLine("Parameter error: {0}", tmp[k]);
                    continue;
                }

                // Suddivido chiave e valore
                var chiave = dati.Substring(0, pos).Trim();
                var valore = dati.Substring(pos + 1).Trim();

                // Memorizzo il dato
                _parClient[idx].AddMod(chiave, valore);

                if (salva)
                {
                    var ini = new GrpUtility.IniFile("server.ini");
                    ini.Scrivi(cookieId.ToString(), chiave, valore);
                }
            }

            return idx;
        }

        /// <summary>
        ///     Restituisce il valore di un parametro
        /// </summary>
        /// <param name="cookieId">Codice del cookie</param>
        /// <param name="chiave">Chiave da cercare</param>
        /// <returns></returns>
        internal static string Get(long cookieId, string chiave)
        {
            // Controllo se il parametro è in cache, se non c'è verifico se è presente nel file server.ini
            // se non c'è, richiamo i parametri di default
            int idx;
            if (!ParClientEsiste(cookieId, out idx) && !ParClientLoad(cookieId, chiave, out idx))
                return ParClientDefault(chiave);

            // Se viene restituito -1 come indice richiamo i parametri di default
            //if (idx == -1)
            //    return ParClientDefault(chiave);

            // Se ho un indice valido, leggo i dati dalla cache
            var valore = _parClient[idx].Get(chiave);
            return !string.IsNullOrEmpty(valore) ? valore : ParClientDefault(chiave);
        }

        /// <summary>
        ///     Verifica se ci sono dati salvati e carica nella cache
        /// </summary>
        /// <param name="cookieId">Codice del cookie</param>
        /// <param name="chiave">Chiave da cercare</param>
        /// <param name="cookieIdx">Indice array della cache</param>
        /// <returns>Indice array posizione dato</returns>
        private static bool ParClientLoad(long cookieId, string chiave, out int cookieIdx)
        {
            // Leggo i dati del client dal file server.ini
            var ini = new GrpUtility.IniFile("server.ini");
            var dato = ini.Leggi(cookieId.ToString(), chiave);

            if (string.IsNullOrEmpty(dato))
            {
                // Se non trovo nulla, restituisco -1
                cookieIdx = -1;
                return false;
            }

            // Memorizzo i dati nella cache e restituisco l'indice
            cookieIdx = Set(cookieId, chiave + "=" + dato);
            return true;
        }

        /// <summary>
        ///     Restituisce i parametri di default
        /// </summary>
        /// <param name="chiave">Chiave da cercare</param>
        /// <returns></returns>
        private static string ParClientDefault(string chiave)
        {
            switch(chiave)
            {
                case "orderknr":
                    return string.Empty;

                case "refresh":
                    return "10";

                case "release":
                    return "false";

                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Inizializzazioni
        /// </summary>
        static CacheParametri()
        {
            _parClient = new Parametro[0];
        }
    }
}
