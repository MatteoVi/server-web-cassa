﻿using System;
using Server.GrpWebServer;
using Server.GrpDatabase;

namespace Server
{
    internal class Avvio
    {
        internal static void Main()
        {
            Console.WriteLine("Server Web Cassa, vers. " + GrpUtility.Varie.LeggeVersioneAttuale());
            Console.WriteLine(string.Empty);

            // Verifica connessione al DB
            Console.WriteLine("Verifico la connessione al DB");
            var conn = new ConnessioneDb();
            if (!conn.TestConnessione())
            {
                Console.WriteLine("Test connessione database FALLITA");
                Console.WriteLine(string.Empty);
                Console.WriteLine("Il server richiede che il database sia attivo, ci sono 2 possibilità:");
                Console.WriteLine("- far partire la cassa in \"multicassa master\"");
                Console.WriteLine("- far partire il DB manualmente tramite: \".\\bin\\fbserver.exe -a -n -p 3000\"");
                Console.WriteLine(string.Empty);
                Console.WriteLine("Premi un tasto per chiudere");
                Console.Read();
                return;
            }

            Console.WriteLine("Test connessione database OK");
            GestioneDb.StringaConnessione = conn.StringaConnessione;

            // Avvio server web
            Console.WriteLine(string.Empty);
            Console.WriteLine("Avvio server web...");
            
            var httpServer = new HttpServerCassa(3002);
            httpServer.Start();

            Console.WriteLine(string.Empty);
            Console.WriteLine("E' possibile collegarsi tramite browser a questi indirizzi:");

            Console.WriteLine("http://localhost:3002/");
            var ip = GrpUtility.Varie.IndirizziIpLocali();
            for (var k = 0; k < ip.Count; k++)
                Console.WriteLine("http://{0}:{1}/", ip[k], "3002");

            Console.WriteLine(string.Empty);
            Console.WriteLine("Assicurarsi che il firewall sia spento oppure che la porta 3002 sia aperta");
            Console.WriteLine(string.Empty);
        }
    }
}