var lastLoad;
var orderK;
var tmrAnn;

$(document)
    .ready(function () {
        $('input[type="checkbox"]')
            .click(function () {
                if ($(this).prop("checked")) 
					{
						$.post("/null", 'act=a_prstse&idticketl=' + $(this).attr('value'));
						$(this).closest("tr").find(".cassa-sts-blue").removeClass("cassa-sts-blue").addClass("cassa-sts-green");
						$(this).closest("tr").find(".fa-clock-o").removeClass("fa-clock-o").addClass("fa-check");
					} 
				else 
					{
						$.post("/null", 'act=a_prstsp&idticketl=' + $(this).attr('value'));
						$(this).closest("tr").find(".cassa-sts-green").removeClass("cassa-sts-green").addClass("cassa-sts-blue");
						$(this).closest("tr").find(".fa-check").removeClass("fa-check").addClass("fa-clock-o");
					}
            });
			
		lastLoad = $('#timestamp').attr('value');
		orderK = $('#orderk').attr('value');
		getAlert();
    });

function getAlert() {
    clearInterval(tmrAnn);
    if (orderK.length > 0) {
        $.get("/orderka.html", {timestamp: lastLoad, orderknr: orderK, now: $.now()}).done(function (data) {
            if (data.indexOf("true") > -1) {location.reload(true);}
        });
        tmrAnn = setInterval(getAlert, 7000);
    }
}

function reload() {
	location.reload(true);
}

function getInput(name, id, page) {
    var key = '#' + id + ' input[type=text]';
	var page = '/' + page;
    var allData = '';
    $(key).each(function () {
        allData += '&' + $(this).attr('name') + '=' + $(this).val();
    });	
    data = 'act=' + name + '&id=' + id + allData;
    $.post(page, data);
    return false;
}
	
function blinker () {
    $('.blink_me').fadeOut(700).fadeIn(700);
}

setInterval(blinker, 2500);