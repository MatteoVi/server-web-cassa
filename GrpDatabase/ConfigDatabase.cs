﻿using System.IO;
using Server.GrpUtility;

namespace Server.GrpDatabase
{
    internal static class ConfigDatabase
    {
        private static readonly IniFile Ini = new IniFile("cassa.ini");

        /// <summary>
        ///     Porta
        /// </summary>
        internal static string Porta;

        /// <summary>
        ///     Utente master
        /// </summary>
        internal static string UserId;

        /// <summary>
        ///     Password master
        /// </summary>
        internal static string Password;

        /// <summary>
        ///     Nome EXE server
        /// </summary>
        internal static string Percorso;

        /// <summary>
        ///     localhost o ip server
        /// </summary>
        internal static string DataSource;

        static ConfigDatabase()
        {
            // Porta
            Porta = Ini.Leggi("Firebird", "Port");
            if (string.IsNullOrEmpty(Porta)) Porta = "3000";

            // Percorso DB
            Percorso = Ini.Leggi("Firebird", "Database");
            if (string.IsNullOrEmpty(Percorso)) Percorso = Directory.GetCurrentDirectory() + "\\cassa.fdb";

            // Utente
            UserId = Ini.Leggi("Firebird", "UserID");
            if (string.IsNullOrEmpty(UserId)) UserId = "sysdba";

            // Password
            Password = Ini.Leggi("Firebird", "Password");
            if (string.IsNullOrEmpty(Password)) Password = "masterkey";

            // Indirizzo IP
            DataSource = Ini.Leggi("Firebird", "Ip");
            if (string.IsNullOrEmpty(DataSource) || DataSource.UgualeA("localhost") || !Varie.VerificaIpV4(DataSource))
                DataSource = "127.0.0.1";
        }
    }
}