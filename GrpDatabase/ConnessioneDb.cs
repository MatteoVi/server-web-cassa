using System;
using FirebirdSql.Data.FirebirdClient;

namespace Server.GrpDatabase
{
    internal class ConnessioneDb
    {
        /// <summary>
        ///     Inizializza la connessione con i parametri standard
        /// </summary>
        internal ConnessioneDb()
        {
            Inizializza(ConfigDatabase.Percorso, ConfigDatabase.DataSource, FbServerType.Default);
        }

        /// <summary>
        ///     Restituisce la stringa di connessione
        /// </summary>
        internal string StringaConnessione;

        /// <summary>
        ///     Creo la stringa di connessione
        /// </summary>
        /// <param name="percorso"></param>
        /// <param name="dataSource"></param>
        /// <param name="serverType"></param>
        private void Inizializza(string percorso, string dataSource, FbServerType serverType)
        {
            var csb = new FbConnectionStringBuilder
            {
                Database = percorso,
                UserID = ConfigDatabase.UserId,
                Password = ConfigDatabase.Password,
                DataSource = dataSource,
                ServerType = serverType,
                Port = Convert.ToInt32(ConfigDatabase.Porta),
                Charset = FbCharset.Utf8.ToString(),
                Dialect = 3,
                ConnectionLifeTime = 15,
                Pooling = true,
                MinPoolSize = 0,
                MaxPoolSize = 50,
                PacketSize = 8192
            };

            StringaConnessione = csb.ToString();
        }

        /// <summary>
        ///     Eseguo un test di lettura e scrittura sul DB
        /// </summary>
        /// <returns></returns>
        private static bool TestScritturaLettura(ref FbConnection myConnection)
        {
            try
            {
                Console.WriteLine("TestConnessione - Test scrittura");
                myConnection.Open();

                // Inserisco un valore nella tabella di configurazione
                var txt = "update or insert into config (seq, grp, data1, dsc) values (2, 0, 'x', '') matching(seq, grp);";
                var myTransaction = myConnection.BeginTransaction("TEST");
                var myCommand = new FbCommand(txt, myConnection, myTransaction);
                myCommand.ExecuteNonQuery();
                myTransaction.Commit();
                myCommand.Dispose();

                // Leggo un valore dalla tabella di configurazione
                Console.WriteLine("TestConnessione - Test lettura");
                txt = "select data1 from config where grp=0 and seq=1;";
                myCommand = new FbCommand(txt, myConnection);
                txt = (string)myCommand.ExecuteScalar();
                myCommand.Dispose();

                return !string.IsNullOrEmpty(txt);
            }
            catch
            {
                return false;
            }
            finally
            {
                myConnection.Close();
                myConnection.Dispose();
            }
        }

        /// <summary>
        ///     Test della connessione
        /// </summary>
        /// <returns></returns>
        internal bool TestConnessione()
        {
            var myConnection = new FbConnection(StringaConnessione);

            try
            {
                return TestScritturaLettura(ref myConnection);
            }
            catch
            {
                return false;
            }
        }
    }
}