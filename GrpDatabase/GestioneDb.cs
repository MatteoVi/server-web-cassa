﻿using System;
using FirebirdSql.Data.FirebirdClient;

namespace Server.GrpDatabase
{
    internal class GestioneDb
    {
        internal static string StringaConnessione;
        private const string Tn = "ServerWeb";

        /// <summary>
        ///     Esegue una query SQL con ritorno TRUE o FALSE
        /// </summary>
        /// <param name="sql">Stringa SQL da processare</param>
        /// <returns>Restituisce TRUE o FALSE</returns>
        internal static bool ExecSql(string sql)
        {
            var newSql = new string[1];
            newSql[0] = sql;
            return ExecSql(ref newSql);
        }


        /// <summary>
        ///     Esegue una query SQL con ritorno TRUE o FALSE
        /// </summary>
        /// <param name="sql">Array di stringhe SQL da processare</param>
        /// <returns>Restituisce TRUE o FALSE</returns>
        internal static bool ExecSql(ref string[] sql)
        {
            var myConnection = new FbConnection(StringaConnessione);
            try
            {
                myConnection.Open();
                var myTransaction = myConnection.BeginTransaction(Tn);

                var num = sql.Length;
                for (byte k = 0; k < num; k++)
                {
                    var myCommand = new FbCommand(sql[k], myConnection, myTransaction);
                    myCommand.ExecuteNonQuery();
                    myCommand.Dispose();
                }
                myTransaction.Commit();
                myTransaction.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("ExecSql - ERRORE - " + ex);
                return false;
            }
            finally
            {
                myConnection.Close();
                myConnection.Dispose();
            }
        }


        /// <summary>
        ///     Leggo data e ora dal database
        /// </summary>
        internal static bool DataOra(out DateTime tsDateTime)
        {
            var myConnection = new FbConnection(StringaConnessione);
            try
            {
                myConnection.Open();
                System.Threading.Thread.Sleep(new Random().Next(1, 5));
                var myCommand = new FbCommand("select current_timestamp from rdb$database;", myConnection);
                var myDataReader = myCommand.ExecuteReader();
                myDataReader.Read();
                tsDateTime = myDataReader.GetDateTime(0);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("DataOra - ERRORE - " + ex);
                tsDateTime = DateTime.Now;
                return false;
            }
            finally
            {
                myConnection.Close();
                myConnection.Dispose();
            }
        }
    }
}