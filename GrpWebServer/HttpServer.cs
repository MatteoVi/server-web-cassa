﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Server.GrpWebServer
{
    internal abstract class HttpServer
    {
        protected volatile bool SegnaleStop;

        private readonly int _porta;
        private readonly Thread _thread;
        private TcpListener _listener;

        protected HttpServer(int porta)
        {
            SegnaleStop = false;

            _porta = porta;
            _listener = null;
            _thread = new Thread(Listen)
            {
                Name = "Server Cassa Web"
            };
        }

        
        /// <summary>
        ///     Loop di ascolto richieste
        /// </summary>
        /// <param name="param"></param>
        private void Listen(object param)
        {
            var errorCount = 0;
            var lastError = DateTime.Now;

            TcpListener listener = null;

            while (!SegnaleStop)
            {
                var threwExceptionOuter = false;
                try
                {
                    listener = new TcpListener(IPAddress.Parse("0.0.0.0"), _porta);

                    _listener = listener;
                    listener.Start();

                    while (!SegnaleStop)
                    {
                        var innerErrorCount = 0;
                        var innerLastError = DateTime.Now;
                        try
                        {
                            var s = listener.AcceptTcpClient();
                            int workerThreads, completionPortThreads;
                            ThreadPool.GetAvailableThreads(out workerThreads, out completionPortThreads);

                            if (workerThreads > 0)
                            {
                                var processor = new HttpProcessor(s, this);
                                ThreadPool.QueueUserWorkItem(processor.Process);
                            }
                            else
                            {
                                var outputStream = new StreamWriter(s.GetStream());
                                outputStream.WriteLine("HTTP/1.1 503 Service Unavailable");
                                outputStream.WriteLine("Connection: close");
                                outputStream.WriteLine("");
                                outputStream.WriteLine("Server too busy");
                            }
                        }

                        catch (ThreadAbortException)
                        {
                            throw;
                        }

                        catch (Exception)
                        {
                            if (DateTime.Now.Hour != innerLastError.Hour || DateTime.Now.DayOfYear != innerLastError.DayOfYear)
                                innerErrorCount = 0;

                            if (++innerErrorCount > 10)
                                throw;

                            Thread.Sleep(1);
                        }
                    }
                }

                catch (ThreadAbortException)
                {
                    SegnaleStop = true;
                }

                catch (Exception)
                {
                    if (DateTime.Now.DayOfYear != lastError.DayOfYear || DateTime.Now.Year != lastError.Year)
                    {
                        lastError = DateTime.Now;
                        errorCount = 0;
                    }
                    if (++errorCount > 100)
                        throw;

                    threwExceptionOuter = true;
                }

                finally
                {
                    try
                    {
                        if (listener != null)
                        {
                            listener.Stop();
                            if (threwExceptionOuter)
                                Thread.Sleep(1000);
                        }
                    }

                    catch (ThreadAbortException)
                    {
                        SegnaleStop = true;
                    }
                    catch (Exception)
                    { }
                }
            }
        }


        /// <summary>
        ///     Avvia il server
        /// </summary>
        internal void Start()
        {
            _thread?.Start(false);
        }


        /// <summary>
        ///     Ferma il server
        /// </summary>
        internal void Stop()
        {
            if (SegnaleStop)
                return;
            SegnaleStop = true;
            if (_listener != null)
                try
                {
                    _listener.Stop();
                }
                catch
                { }

            if (_thread != null)
                try
                {
                    _thread.Abort();
                }
                catch
                { }

            try
            {
                GestisceStop();
            }
            catch
            { }
        }


        /// <summary>
        ///     Gestisce i GET
        /// </summary>
        /// <param name="p"></param>
        internal abstract void GestisceGet(HttpProcessor p);

        /// <summary>
        ///     Gestisce i POST
        /// </summary>
        /// <param name="p"></param>
        /// <param name="inputData"></param>
        internal abstract void GestiscePost(HttpProcessor p, StreamReader inputData);

        /// <summary>
        ///     Gestisce lo Stop
        /// </summary>
        internal abstract void GestisceStop();
    }
}




