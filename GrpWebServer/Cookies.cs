using System;
using Server.GrpUtility;

namespace Server.GrpWebServer
{
    /// <summary>
    ///     Cache dei cookies
    /// </summary>
    internal class Cookies
    {
        private Cookie[] _item;

        /// <summary>
        ///     Numero coolies effettivamente presenti
        /// </summary>
        private int _dim;

        /// <summary>
        ///     Aggiunge un cookie con durata infinita
        /// </summary>
        /// <param name="nome">Nome</param>
        /// <param name="valore">Valore</param>
        internal void Add(string nome, string valore)
        {
            Add(nome, valore, TimeSpan.MaxValue);
        }

        /// <summary>
        ///     Aggiunge un cookie
        /// </summary>
        /// <param name="nome">Nome</param>
        /// <param name="valore">Valore</param>
        /// <param name="durata">Durata</param>
        internal void Add(string nome, string valore, TimeSpan durata)
        {
            if (string.IsNullOrEmpty(nome))
                return;

            _dim++;

            if (_dim == _item.Length)
                Array.Resize(ref _item, _dim + 5);

            _item[_dim] = new Cookie(nome, valore, durata);
        }

        /// <summary>
        ///     Restituisce il valore partendo dal nome
        /// </summary>
        /// <param name="nome">Nome in MINUSCOLO</param>
        /// <returns></returns>
        internal string Get(string nome)
        {
            for (var k = 0; k <= _dim; k++)
                if (nome.UgualeA(_item[k].NomeL))
                    return _item[k].Valore;

            return string.Empty;
        }


        /// <summary>
        ///     Cancella
        /// </summary>
        /// <param name="nome">Nome in MINUSCOLO</param>
        /// <returns></returns>
        internal void Del(string nome)
        {
            for (var k = 0; k < _dim; k++)
                if (nome.UgualeA(_item[k].NomeL))
                { 
                    // Dico al coockie di durare 1 millisecondo
                    _item[k].Fine = TimeSpan.FromMilliseconds(1);
                    break;
                }
        }

        /// <summary>
        ///     Restituisce i cookies su una stringa unica 
        /// </summary>
        /// <returns></returns>
        internal string StringaUnica()
        {
            if (_dim == -1)
                return string.Empty;

            var str = new string[_dim + 1];

            for (var k = 0; k <= _dim; k++)
                str[k] = "Set-Cookie: " + _item[k].Nome + "=" + _item[k].Valore +
                         (_item[k].Fine == TimeSpan.Zero ? string.Empty : "; Max-Age=" + (long)_item[k].Fine.TotalSeconds) + "; Path=/";

            return string.Join("\r\n", str);
        }

        /// <summary>
        ///     Inizializzazione
        /// </summary>
        internal Cookies(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                _dim = -1;
                _item = new Cookie[0];
                return;
            }

            var cookies = str.Split(';');

            _dim = cookies.Length;
            _item = new Cookie[_dim];
            _dim--;

            for (var k = 0; k <= _dim; k++)
            {
                var pos = cookies[k].IndexOf('=');
                if (pos < 1)
                    continue;

                var nome = cookies[k].Substring(0, pos).Trim();
                var valore = cookies[k].Substring(pos + 1).Trim();

                _item[k] = new Cookie(nome, valore, TimeSpan.MaxValue);
            }
        }
    }
}