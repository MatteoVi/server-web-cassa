using System;
using System.IO;
using System.Reflection;
using Server.GrpComandi;
using Server.GrpUtility;

namespace Server.GrpWebServer
{
    internal class HttpServerCassa : HttpServer
    {
        internal long CookieId;

        internal HttpServerCassa(int port) : base(port)
        {
            var path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\web";
            Directory.CreateDirectory(path);
        }

        /// <summary>
        ///     Sistema il path da richiamare
        /// </summary>
        /// <param name="p"></param>
        private static void NormalizzaPath(ref HttpProcessor p)
        {
            if (p.HttpUrl != "web/" && p.HttpUrl != "/")
                return;

            if (File.Exists("web/index.html"))
                p.HttpUrl = "web/index.html";
            else
            {
                p.WriteHeader200("text/html", false);
                p.OutputStream.WriteLine("<h1>OK...</h1>");
            }
        }

        /// <summary>
        ///     Recupero l'id univoco di sessione
        /// </summary>
        /// <param name="p"></param>
        private void LeggiSessionId(ref HttpProcessor p)
        {
            if (long.TryParse(p.Cookies.Get("CassaId"), out CookieId))
                return;

            CookieId = Varie.TimeStamp();
            p.Cookies.Add("CassaId", CookieId.ToString());
        }

        /// <summary>
        ///     Quando ricevo il GET
        /// </summary>
        /// <param name="p"></param>
        internal override void GestisceGet(HttpProcessor p)
        {
            NormalizzaPath(ref p);
            LeggiSessionId(ref p);
            CaricaPagina(ref p);
        }


        /// <summary>
        ///     Quando ricevo il POST
        /// </summary>
        /// <param name="p"></param>
        /// <param name="inputData"></param>
        internal override void GestiscePost(HttpProcessor p, StreamReader inputData)
        {
            NormalizzaPath(ref p);
            LeggiSessionId(ref p);
            GestisciParametriPost(inputData.ReadToEnd());
            CaricaPagina(ref p);
        }

        /// <summary>
        ///     Quando ricevo segnale di stop
        /// </summary>
        internal override void GestisceStop()
        { }

        /// <summary>
        ///     Gestisco i parametri che ricevo dal POST
        /// </summary>
        /// <param name="dati">Stringa dati ricevuta</param>
        private void GestisciParametriPost(string dati)
        {
            Console.WriteLine("Parameters: {0}", dati);

            if (string.IsNullOrEmpty(dati))
                return;

            string act;
            if (dati.TrovaValore("act", out act))
                ElaboraAzione(ref act, ref dati);
            else
                GrpCache.CacheParametri.Set(CookieId, dati);
        }

        /// <summary>
        ///     Gestisco i parametri che ricevo dalla riga
        /// </summary>
        /// <param name="url">Url</param>
        /// <param name="parUrl">Parametro passato nell'url</param>
        private void GestisciParametriRiga(ref string url, out string parUrl)
        {
            // Inizializzo i parametri a vuoto
            parUrl = string.Empty;

            // Verifico se ci sono i parametri
            var pos = url.IndexOf('?');
            if (pos == -1)
                return;

            // Divido Url dai parametri
            var dati = url.Substring(pos + 1);
            url = url.Substring(0, pos);

            // Se c'� parametro "act" elaboro l'azione...
            string act;
            if (dati.TrovaValore("act", out act))
            {
                ElaboraAzione(ref act, ref dati);
                return;
            }

            // ... altrimenti ritorno i parametri
            parUrl = dati;
        }

        /// <summary>
        ///     In base all'estensione processo le pagine in modo appro
        /// </summary>
        /// <param name="p"></param>
        private void CaricaPagina(ref HttpProcessor p)
        {
            string parUrl;
            GestisciParametriRiga(ref p.HttpUrl, out parUrl);

            // Trova l'ID se c'�
            var pos = p.HttpUrl.IndexOf('#');

            if (pos == -1 && !File.Exists(p.HttpUrl))
                    return;

            if (pos != -1 && !File.Exists(p.HttpUrl.Substring(0, pos)))
                return;

            if (p.HttpUrl.Contains(".htm"))
            {
                p.WriteHeader200("text/html", true);
                LeggiHtml(ref p, ref parUrl);
            }

            else if (p.HttpUrl.Contains(".css"))
            {
                p.WriteHeader200("text/css", false);
                LeggiTxt(ref p);
            }

            else if (p.HttpUrl.Contains(".js"))
            {
                p.WriteHeader200("text/javascript", false);
                LeggiTxt(ref p);
            }

            else if (p.HttpUrl.Contains(".eot") || p.HttpUrl.Contains(".woff") || p.HttpUrl.Contains(".ttf"))
            {
                p.WriteHeader200("application/octet-stream", false);
                LeggiBin(ref p);
            }

            else if (p.HttpUrl.Contains(".png"))
            {
                p.WriteHeader200("image/png", false);
                LeggiBin(ref p);
            }

            else if (p.HttpUrl.Contains(".jpg"))
            {
                p.WriteHeader200("image/jpg", false);
                LeggiBin(ref p);
            }

            else if (p.HttpUrl.Contains(".gif"))
            {
                p.WriteHeader200("image/gif", false);
                LeggiBin(ref p);
            }

            else if (p.HttpUrl.Contains(".tif"))
            {
                p.WriteHeader200("image/tiff", false);
                LeggiBin(ref p);
            }

            else if (p.HttpUrl.Contains(".ico"))
            {
                p.WriteHeader200("image/ico", false);
                LeggiBin(ref p);
            }
        }

        /// <summary>
        ///     Legge ed interpreta i file html con codici di cassa
        /// </summary>
        /// <param name="p"></param>
        /// <param name="parUrl"></param>
        private void LeggiHtml(ref HttpProcessor p, ref string parUrl)
        {
            string line;
            using (var sr = new StreamReader(p.HttpUrl))
                while ((line = sr.ReadLine()) != null)
                {
                    // Trasformo la stringa in minuscolo e cerco il codice "<cassa"
                    var idx1 = line.ToLower().IndexOf("<cassa", StringComparison.OrdinalIgnoreCase);

                    // Se non c'� il codice cassa, invio il dato
                    if (idx1 == -1)
                    {
                        p.OutputStream.WriteLine(line);
                        continue;
                    }

                    // Ho trovato il codice.....

                    // Invio la prima parte della riga
                    p.OutputStream.WriteLine(line.Substring(0, idx1));

                    // Incremento il contatore per saltare "<cassa"
                    idx1 += 7;

                    // Prendo il pezzo di stringa rimanente
                    var cmd = line.Substring(idx1);

                    // Trovo il carattere di chiusura del comando ">"
                    var idx2 = cmd.IndexOf('>');

                    // Ottengo la stringa della funzione da richiamare con i relativi parametri, trimmo e rendo tutto minuscolo
                    cmd = cmd.Substring(0, idx2).Trim().ToLower();

                    // Elaboro il comando e mando al browser il risultato
                    p.OutputStream.WriteLine(ElaboraTagCassa(ref cmd, ref parUrl));

                    // Mando al browser il resto della linea
                    idx1 += idx2 + 1;
                    if (line.Length >= idx1)
                        p.OutputStream.WriteLine(line.Substring(idx1));
                }

            p.OutputStream.Flush();
        }

        /// <summary>
        ///     Legge ed invia i file html "piatti"
        /// </summary>
        /// <param name="p"></param>
        private static void LeggiTxt(ref HttpProcessor p)
        {
            string line;
            using (var sr = new StreamReader(p.HttpUrl))
                while ((line = sr.ReadLine()) != null)
                    p.OutputStream.WriteLine(line);

            p.OutputStream.Flush();
        }

        /// <summary>
        ///     Legge ed invia i dati binari
        /// </summary>
        /// <param name="p"></param>
        private static void LeggiBin(ref HttpProcessor p)
        {
            using (var fs = File.Open(p.HttpUrl, FileMode.Open))
            {
                var buffer = new byte[fs.Length];
                int bytesRead;

                while ((bytesRead = fs.Read(buffer, 0, buffer.Length)) > 0)
                    p.OutputStream.BaseStream.Write(buffer, 0, bytesRead);
            }

            p.OutputStream.BaseStream.Flush();
        }

        /// <summary>
        ///     Elaboro il tag cassa ricevuto
        /// </summary>
        /// <param name="cmdStr">Stringa comando e parametri</param>
        /// <param name="parUrl">Parametro passato nell'url</param>
        /// <returns></returns>
        private string ElaboraTagCassa(ref string cmdStr, ref string parUrl)
        {
            if (string.IsNullOrEmpty(cmdStr))
                return string.Empty;

            var pos = cmdStr.IndexOf(',');
            var cmd = cmdStr;
            var parTag = string.Empty;

            if (pos != -1)
            {
                cmd = cmdStr.Substring(0, pos);
                parTag = cmdStr.Substring(pos + 1).Trim();
            }
            
            switch (cmd)
            {
                case "orderk":
                    var cp = new ComandePag();
                    return cp.ElaboraComande(ref CookieId, ref parTag, ref parUrl);

                case "orderka":
                    var ca = new ComandeAnn();
                    return ca.ElaboraComande(ref CookieId, ref parUrl);

                case "version":
                    return Generici.Versione();

                case "timestamp":
                    return Generici.ServerTimeStamp();

                case "setupk":
                    var setup = new Setupk();
                    return setup.CreaPagina(ref CookieId);

                case "orderknr":
                    var lc = new ListaChiavi();
                    return lc.Lista(CookieId, parTag, cmd);

                default:
                    Console.WriteLine("Command not recongnised: " + cmd);
                    break;
            }

            return string.Empty;
        }

        /// <summary>
        ///     Elaboro l'azione
        /// </summary>
        /// <param name="act">Nome azione da compiere</param>
        /// <param name="dati">Parametri</param>
        private void ElaboraAzione(ref string act, ref string dati)
        {
            // Creo una istanza delle azioni
            var az = new Azioni(ref dati, ref CookieId);

            // Gestisco l'azione
            act = act.ToLower();
            switch (act)
            {
                case "a_screm":
                    az.PrdUpdStatoChiudi();
                    break;

                case "a_scena":
                    az.PrdUpdStatoP();
                    break;

                case "a_sctabl":
                    az.UpdTavolo();
                    break;

                case "a_prstse":
                    az.PrdUpdStato('E');
                    break;

                case "a_prstsp":
                    az.PrdUpdStato('P');
                    break;

                case "a_setpar":
                    az.ScriviParametri(false);
                    break;

                case "a_setparf":
                    az.ScriviParametri(true);
                    break;

                default:
                    Console.WriteLine("Action not recongnised: " + act);
                    break;
            }
        }
    }
}