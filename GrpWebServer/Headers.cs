﻿using System;

namespace Server.GrpWebServer
{
    /// <summary>
    ///     Cache degli headers
    /// </summary>
    internal struct Headers
    {
        private Header[] _item;

        /// <summary>
        ///     Numero headers effettivamente presenti
        /// </summary>
        private int _dim;

        /// <summary>
        ///     Aggiunge un header
        /// </summary>
        /// <param name="chiave">Chiave</param>
        /// <param name="valore">Valore</param>
        internal void Add(string chiave, string valore)
        {
            if (string.IsNullOrEmpty(chiave))
                return;

            _dim++;

            if (_dim == _item.Length)
                Array.Resize(ref _item, _dim + 5);

            _item[_dim] = new Header(chiave, valore);
        }

        /// <summary>
        ///     Restituisce il valore partendo dalla chiave
        /// </summary>
        /// <param name="chiave">Chiave da cercare in MINUSCOLO</param>
        /// <returns></returns>
        internal string Get(string chiave)
        {
            for (var k = 0; k <= _dim; k++)
                if (string.Compare(chiave, _item[k].ChiaveL, StringComparison.OrdinalIgnoreCase) == 0)
                    return _item[k].Valore;

            return string.Empty;
        }

        /// <summary>
        ///     Inizializzazione
        /// </summary>
        internal Headers(byte num)
        {
            _dim = -1;
            _item = new Header[num];
        }
    }

}
