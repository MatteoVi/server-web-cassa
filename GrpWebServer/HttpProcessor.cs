﻿using System;
using System.IO;
using System.Net.Sockets;

namespace Server.GrpWebServer
{
    internal class HttpProcessor
    {
        /// <summary>
        ///     Il client tcp che accetta le connessioni
        /// </summary>
        internal TcpClient ClientTcp;

        /// <summary>
        ///     Il server http che gestisce la richiesta
        /// </summary>
        internal HttpServer Srv;

        /// <summary>
        ///     Lo stream in input
        /// </summary>
        private Stream _inputStream;

        /// <summary>
        ///     Lo stream in output
        /// </summary>
        internal StreamWriter OutputStream;

        /// <summary>
        ///     Cookies
        /// </summary>
        internal Cookies Cookies;

        /// <summary>
        ///     Testate documento
        /// </summary>
        internal Headers Header;

        /// <summary>
        ///     Metodo
        /// </summary>
        internal string HttpMethod;

        /// <summary>
        ///     Url
        /// </summary>
        internal string HttpUrl;

        /// <summary>
        ///     Versione protocollo
        /// </summary>
        internal string HttpProtocolVersionstring;

        /// <summary>
        ///     Limite massimo post
        /// </summary>
        private static readonly int _maxPostSize =  1024 * 10;

        internal HttpProcessor(TcpClient s, HttpServer srv)
        {
            ClientTcp = s;
            Srv = srv;
        }

        private static string StreamReadLine(Stream inputStream)
        {
            var data = string.Empty;
            while (true)
            {
                var nextChar = inputStream.ReadByte();
                if (nextChar == '\n')
                    break;

                if (nextChar == '\r')
                    continue;

                if (nextChar == -1)
                {
                    System.Threading.Thread.Sleep(1); // Per limitare l'uso della CPU
                    continue;
                }

                data += Convert.ToChar(nextChar);
            }

            return data;
        }


        internal void Process(object objParameter)
        {
            var tcpStream = ClientTcp.GetStream();

            _inputStream = new BufferedStream(tcpStream);
            OutputStream = new StreamWriter(new BufferedStream(tcpStream));

            try
            {
                ParseRequest();
                ReadHeaders();

                Cookies = new Cookies(Header.Get("cookie"));

                if (HttpMethod.Equals("GET"))
                    HandleGetRequest();

                else if (HttpMethod.Equals("POST"))
                    HandlePostRequest();

                else
                    Console.WriteLine("HttpMethod not handled: " + HttpMethod);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e);
                WriteHeader404();
            }

            try
            {
                OutputStream.Flush();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e);
            }

            OutputStream = null;
            _inputStream = null;

            try
            {
                ClientTcp?.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e);
            }

            try
            {
                tcpStream.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e);
            }
        }

        internal void ParseRequest()
        {
            var request = StreamReadLine(_inputStream);
            var tokens = request.Split(' ');

            if (tokens.Length != 3)
                throw new Exception("invalid http request line");
            
            HttpMethod = tokens[0].ToUpper();
            HttpUrl = "web" + tokens[1];
            HttpProtocolVersionstring = tokens[2];

            Console.WriteLine(request);
        }

        internal void ReadHeaders()
        {
            //Console.WriteLine("readHeaders()");
            Header = new Headers(15);

            string line;
            while ((line = StreamReadLine(_inputStream)) != null)
            {
                if (line.Equals(string.Empty))
                {
                    //Console.WriteLine("got headers");
                    return;
                }

                var separator = line.IndexOf(':');
                if (separator == -1)
                {
                    throw new Exception("invalid http header line: " + line);
                }
                var nome = line.Substring(0, separator);
                var pos = separator + 1;
                while ((pos < line.Length) && (line[pos] == ' '))
                    pos++; // strip any spaces

                var valore = line.Substring(pos, line.Length - pos);
                // Console.WriteLine("header: {0}:{1}", nome, valore);
                Header.Add(nome, valore);
            }
        }

        internal void HandleGetRequest()
        {
            Srv.GestisceGet(this);
        }

        private const int BufSize = 4096;
        internal void HandlePostRequest()
        {
            //Console.WriteLine("get post data start");

            var ms = new MemoryStream();
            var cls = Header.Get("content-length");

            if (!string.IsNullOrEmpty(cls))
            {
                int cl;
                if (int.TryParse(cls, out cl))
                {
                    if (cl > _maxPostSize)
                        throw new Exception($"POST Content-Length({cl}) too big");

                    var buf = new byte[BufSize];
                    while (cl > 0)
                    {
                        //Console.WriteLine("starting Read, to_read={0}", cl);

                        var numread = _inputStream.Read(buf, 0, Math.Min(BufSize, cl));
                        //Console.WriteLine("read finished, numread={0}", numread);
                        if (numread == 0)
                        {
                            if (cl == 0)
                                break;

                            throw new Exception("client disconnected during post");
                        }
                        cl -= numread;
                        ms.Write(buf, 0, numread);
                    }
                }
                ms.Seek(0, SeekOrigin.Begin);
            }
            //Console.WriteLine("get post data end");
            Srv.GestiscePost(this, new StreamReader(ms));
        }


        internal string GetIp()
        {
            // Recupero l'indirizzo IP del client
            var rep = ClientTcp.Client.RemoteEndPoint;
            if (rep.AddressFamily != AddressFamily.InterNetwork)
                return string.Empty;

            var ip = rep.ToString();
            Console.WriteLine("Client ip address: {0}", ip);
            return ip.Substring(0, ip.IndexOf(':'));
        }

        /// <summary>
        ///     Invia header con codice 200
        /// </summary>
        /// <param name="contentType">Content type</param>
        /// <param name="completo">Se invio i dati di header completi</param>
        /// <param name="gzip">Compressione GZip</param>
        internal void WriteHeader200(string contentType, bool completo, bool gzip=false)
        {
            OutputStream.WriteLine("HTTP/1.0 200 OK");
            OutputStream.WriteLine("Content-Type: " + contentType);
            //OutputStream.WriteLine("Cache-Control: no-transform,public,max-age=60,s-maxage=60");

            if (completo)
            {
                var cookieStr = Cookies.StringaUnica();
                if (!string.IsNullOrEmpty(cookieStr))
                    OutputStream.WriteLine(cookieStr);
            }

            if (gzip)
                OutputStream.WriteLine("Content-Encoding: gzip");

            OutputStream.WriteLine("Connection: close");
            OutputStream.WriteLine(string.Empty);
        }

        /// <summary>
        ///     Invia header con codice 302
        /// </summary>
        internal void WriteHeader302(string location)
        {
            OutputStream.WriteLine("HTTP/1.0 302 REDIRECT");
            OutputStream.WriteLine("Location: " + location);
            OutputStream.WriteLine("Connection: close");
            OutputStream.WriteLine(string.Empty);
        }

        /// <summary>
        ///     Invia header con codice 404
        /// </summary>
        internal void WriteHeader404()
        {
            OutputStream.WriteLine("HTTP/1.0 404 Page not found");
            OutputStream.WriteLine("Connection: close");
            OutputStream.WriteLine(string.Empty);
        }
    }
}