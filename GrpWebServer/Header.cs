﻿namespace Server.GrpWebServer
{
    /// <summary>
    ///     Header
    /// </summary>
    internal struct Header
    {
        internal string Chiave;
        internal string ChiaveL;
        internal string Valore;

        internal Header(string chiave, string valore)
        {
            Chiave = chiave;
            ChiaveL = chiave.ToLower();
            Valore = valore;
        }
    }
}
