﻿using System;

namespace Server.GrpWebServer
{
    /// <summary>
    ///    Cookie
    /// </summary>
    internal struct Cookie
    {
        internal string Nome;
        internal string NomeL;
        internal string Valore;
        internal TimeSpan Fine;

        internal Cookie(string nome, string valore, TimeSpan fine)
        {
            Nome = nome;
            NomeL = nome.ToLower();
            Valore = valore;
            Fine = fine;
        }
    }
}