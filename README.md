# Cos'è #

E' un piccolo server web a complemento del software Gestione Cassa (http://www.gestionecassa.com)

# Installazione #
- Posizionare il programma compilato vella stessa cartella in cui è presente cassa.exe
- Copiare la cartella **WEB** che si trova in "Altro"  nella stessa cartella in cui si trova cassa.exe

Struttura:



    cartella cassa
		|- cassa.exe
		|- server.exe
		|- ....
		|- FirebirdSql.Data.FirebirdClient.dll
		|- ....
		\- WEB
		    |- css
			|- .....
			|- index.html


- Nel caso serva il file FirebirdSql.Data.FirebirdClient.dll, è presente nella cartella "Altro"



# Riconoscimenti #

Questo software si basa sui progetti:

Simple HTTP Server in C# di David Jeske (http://www.codeproject.com/Articles/137979/Simple-HTTP-Server-in-C)


Camera proxy di Brian Pearce (https://cameraproxy.codeplex.com/)