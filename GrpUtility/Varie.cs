﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;

namespace Server.GrpUtility
{
    internal static class Varie
    {
        /// <summary>
        ///     Reperisce l'indirizzo Ip locale della scheda attiva
        /// </summary>
        /// <returns></returns>
        internal static List<string> IndirizziIpLocali()
        {
            var localIp = new List<string>();
            var host = Dns.GetHostEntry(Dns.GetHostName());
            for (var i = 0; i < host.AddressList.Length; i++)
            {
                var ip = host.AddressList[i];
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                    localIp.Add(ip.ToString());
            }

            return localIp;
        }

        /// <summary>
        ///     Verifica se l'indirizzo IP è un indirizzo corretto
        /// </summary>
        /// <param name="ip">Indirizzo da testare</param>
        /// <returns>True o False</returns>
        internal static bool VerificaIpV4(string ip)
        {
            IPAddress res;
            ip = ip.Trim();
            return !string.IsNullOrEmpty(ip) && IPAddress.TryParse(ip, out res);
        }

        /// <summary>
        ///     Verifica se due stringhe sono uguali
        /// </summary>
        /// <param name="str">Stringa</param>
        /// <param name="text">Testo da confrontare</param>
        internal static bool UgualeA(this string str, string text)
        {
            return string.Compare(str, text.Trim(), StringComparison.OrdinalIgnoreCase) == 0;
        }

        /// <summary>
        ///     Verifica se due stringhe sono diverse
        /// </summary>
        /// <param name="str">Stringa</param>
        /// <param name="text">Testo da confrontare</param>
        internal static bool DiversoDa(this string str, string text)
        {
            return !UgualeA(str, text);
        }

        /// <summary>
        ///     Legge la versione attuale del programmma
        /// </summary>
        internal static string LeggeVersioneAttuale()
        {
            var ver = Assembly.GetExecutingAssembly().GetName().Version;
            var vMajor = ver.Major;
            var vMinor = ver.Minor;
            var vBuild = ver.Build;
            var vRevision = ver.Revision;

            return vMajor + "." + vMinor + "." + vBuild + " rev. " + vRevision;
        }

        /// <summary>
        ///     Converte un valore booleano e restituisce T o F
        /// </summary>
        internal static string BoolToDb(this object valore)
        {
            return Convert.ToBoolean(valore) ? "T" : "F";
        }

        /// <summary>
        ///     Converte T o F e restituisce true o false
        /// </summary>
        internal static bool BoolFromDb(this object valore)
        {
            return Convert.ToString(valore).UgualeA("T");
        }

        /// <summary>Crea una marca temporale basata su data e ora attuali</summary>
        internal static long TimeStamp()
        {
            Thread.Sleep(new Random().Next(1, 5));
            return TimeStamp(DateTime.Now);
        }

        /// <summary>Crea una marca temporale con una data e ora passati come parametro</summary>
        /// <param name="valore">Converte un valore specifico</param>
        internal static long TimeStamp(DateTime valore)
        {
            var codice = (long)(valore.Year * 100 + valore.Month) * 100 + valore.Day;
            codice = (((codice * 100 + valore.Hour) * 100 + valore.Minute) * 100 + valore.Second) * 1000 + valore.Millisecond;
            return codice;
        }

        /// <summary>
        ///     Trova il valore di una chiave
        /// </summary>
        /// <param name="parametri">Stringa parametri</param>
        /// <param name="chiave">Chiave IN MINUSCOLO</param>
        /// <param name="valore">Valore</param>
        /// <returns></returns>
        internal static bool TrovaValore(this string parametri, string chiave, out string valore)
        {
            // Imposto il valore di ritorno a stringa vuota
            valore = string.Empty;

            // Lunghezza della chiave cercata
            var dimC = chiave.Length;

            // Lunghezza della stringa dei parametri
            var dimP = parametri.Length;

            // Protezione dimensione chiave maggiore lunghezza parametri
            if (dimC > dimP)
                return false;

            // Verifico se è presente "=" nella chiave, nel caso non ci sia lo aggiungo e adeguo la lunghezza della chiave 
            if (chiave[dimC - 1] != '=')
            {
                chiave += "=";
                dimC++;
            }

            // Se non c'è la chiave cercata, esco
            var posC = parametri.ToLower().IndexOf(chiave, StringComparison.Ordinal);
            if (posC == -1)
                return false;

            // Se la posizione iniziale del valore coincide con la lunghezza del parametro implica valore nullo
            posC += dimC;
            if (dimP == posC)
                return true;

            // Ho la posizione di inizio del valore, 
            // cerco il successivo "&" per determinare la posizione di fine del valore
            int endV;
            for (endV = posC; endV < dimP; endV++)
                if (parametri[endV] == '&')
                    break;

            // Ricavo il valore
            valore = parametri.Substring(posC, endV - posC);
            return true;
        }
    }
}
