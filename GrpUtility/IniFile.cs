﻿using System.Runtime.InteropServices;

namespace Server.GrpUtility
{
    /// <summary>
    ///     Gestisco i file ini
    /// </summary>
    internal class IniFile
    {
        private readonly string _path;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, byte[] retVal, int size, string filePath);

        /// <summary>
        ///     Inizializzazione
        /// </summary>
        /// <param name="path">Percorso file INI</param>
        public IniFile(string path)
        {
            _path = System.IO.Path.GetFullPath(path);
        }

        /// <summary>
        ///     Leggo dal file INI
        /// </summary>
        /// <param name="sezione">Sezione</param>
        /// <param name="chiave">Chiave</param>
        /// <returns></returns>
        public string Leggi(string sezione, string chiave)
        {
            byte[] buffer = new byte[4096];
            var len = GetPrivateProfileString(sezione, chiave, string.Empty, buffer, 4096, _path);
            return len == 0 ? string.Empty : System.Text.Encoding.ASCII.GetString(buffer, 0, len);
        }

        /// <summary>
        ///     Legge tutte le chiavi della sezione
        /// </summary>
        /// <param name="sezione">Sezione</param>
        /// <returns></returns>
        public string[] ListaChiavi(string sezione)
        {
            byte[] buffer = new byte[4096];
            var len = GetPrivateProfileString(sezione, null, null, buffer, 4096, _path);
            return len == 0 ? new string[0] : System.Text.Encoding.ASCII.GetString(buffer, 0, len).Split('\0');
        }

        /// <summary>
        ///     Cancella chiave
        /// </summary>
        /// <param name="sezione">Sezione</param>
        /// <param name="chiave">Chiave</param>
        public void CancellaChiave(string sezione, string chiave)
        {
            WritePrivateProfileString(sezione, chiave, null, _path);
        }

        /// <summary>
        ///     Cancella sezione
        /// </summary>
        /// <param name="sezione">Sezione</param>
        public void CancellaSezione(string sezione)
        {
            WritePrivateProfileString(sezione, null, null, _path);
        }

        /// <summary>
        ///     Scrive i dati nel file INI
        /// </summary>
        /// <param name="sezione">Sezione</param>
        /// <param name="chiave">Chiave</param>
        /// <param name="valore">Valore</param>
        public void Scrivi(string sezione, string chiave, string valore)
        {
            ScriviIni(sezione, chiave, valore);
        }

        /// <summary>
        ///     Scrive i dati nel file INI
        /// </summary>
        /// <param name="sezione">Sezione</param>
        /// <param name="chiave">Chiave</param>
        /// <param name="valore">Valore</param>
        public void Scrivi(string sezione, string chiave, int valore)
        {
            ScriviIni(sezione, chiave, valore.ToString());
        }

        /// <summary>
        ///     Scrive i dati nel file INI
        /// </summary>
        /// <param name="sezione">Sezione</param>
        /// <param name="chiave">Chiave</param>
        /// <param name="valore">Valore</param>
        public void Scrivi(string sezione, string chiave, byte valore)
        {
            ScriviIni(sezione, chiave, valore.ToString());
        }

        private void ScriviIni(string sezione, string chiave, string valore)
        {
            WritePrivateProfileString(sezione, chiave, valore, _path);
        }
    }
}